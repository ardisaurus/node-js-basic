const assert = require("assert");
const mongoose = require("mongoose");
const author = require("../model/authorchar");
describe("Relational author data", function() {
  beforeEach(function(done) {
    mongoose.connection.dropCollection("authorchars", function() {
      done();
    });
  });
  it("Create new author", function(done) {
    var newauthor = new author({
      name: "Patrick",
      books: [{ title: "Wind", pages: 300 }]
    });
    newauthor.save().then(function() {
      author.findOne({ name: "Patrick" }).then(function(result) {
        assert(result.books.length == 1);
        done();
      });
    });
  });
  it("Create add book to author", function(done) {
    var newauthor = new author({
      name: "Patrick",
      books: [{ title: "Wind", pages: 300 }]
    });
    newauthor.save().then(function() {
      author.findOne({ name: "Patrick" }).then(function(result) {
        result.books.push({ title: "Fire", pages: 5000 });
        result.save().then(function() {
          author.findOne({ name: "Patrick" }).then(function(result) {
            assert(result.books.length == 2);
            done();
          });
        });
      });
    });
  });
});
