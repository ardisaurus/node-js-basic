const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

before(function(done) {
  mongoose.connect(
    "mongodb://localhost/coba",
    { useNewUrlParser: true }
  );
  mongoose.connection
    .once("open", function() {
      console.log("Connection estabilished");
      done();
    })
    .on("error", function(error) {
      console.log("Error : ", error);
    });
});
beforeEach(function(done) {
  mongoose.connection.dropCollection("mariochars", function() {
    done();
  });
});
