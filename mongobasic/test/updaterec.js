const marioChar = require("../model/mariochar");
const assert = require("assert");
describe("Update data", function() {
  var char;
  beforeEach(function(done) {
    char = new marioChar({ name: "mario", weight: 50 });
    char.save().then(function() {
      done();
    });
  });
  it("Update data on DB", function(done) {
    marioChar
      .findOneAndUpdate({ name: "mario" }, { name: "luigi" })
      .then(function() {
        marioChar.findOne({ _id: char._id }).then(function(result) {
          assert(result.name == "luigi");
          done();
        });
      });
  });
  it("Increase value of data on DB", function(done) {
    marioChar.findOneAndUpdate({}, { $inc: { weight: 1 } }).then(function() {
      marioChar.findOne({ _id: char._id }).then(function(result) {
        assert(result.weight == 51);
        done();
      });
    });
  });
});
