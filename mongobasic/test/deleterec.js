const marioChar = require("../model/mariochar");
const assert = require("assert");
describe("Delete data", function() {
  var char;
  beforeEach(function(done) {
    char = new marioChar({ name: "maria" });
    char.save().then(function() {
      done();
    });
  });
  it("Delete data on DB", function(done) {
    marioChar.findOneAndDelete({ name: "maria" }).then(function() {
      marioChar.findOne({ name: "maria" }).then(function(result) {
        assert(result == null);
        done();
      });
    });
  });
});
