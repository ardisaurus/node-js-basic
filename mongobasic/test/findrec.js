const marioChar = require("../model/mariochar");
const assert = require("assert");
describe("Find data", function() {
  var char;
  beforeEach(function(done) {
    char = new marioChar({ name: "maria" });
    char.save().then(function() {
      done();
    });
  });
  it("Find data in DB", function(done) {
    marioChar.findOne({ name: "maria" }).then(function(result) {
      assert(result.name == "maria");
      done();
    });
  });
  it("Find data by id in DB", function(done) {
    marioChar.findOne({ _id: char._id }).then(function(result) {
      assert(result._id.toString() == char._id.toString());
      done();
    });
  });
});
