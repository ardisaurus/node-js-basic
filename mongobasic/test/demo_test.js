const assert = require("assert");
const marioChar = require("../model/mariochar");
describe("Saving data", function() {
  it("Saving data to database", function(done) {
    var char = new marioChar({ name: "Maria" });
    char.save().then(function() {
      assert(char.isNew == false);
      done();
    });
  });
});
