const mongoose = require("mongoose");
const scheme = mongoose.Schema;

const marioScheme = new scheme({
  name: String,
  weight: Number
});
const MarioChar = mongoose.model("marioChar", marioScheme);
module.exports = MarioChar;
