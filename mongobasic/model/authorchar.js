const mongoose = require("mongoose");
const scheme = mongoose.Schema;

const bookScheme = new scheme({
  title: String,
  pages: Number
});
const authorScheme = new scheme({
  name: String,
  ages: Number,
  books: [bookScheme]
});
const AuthorChar = mongoose.model("authorChar", authorScheme);
module.exports = AuthorChar;
