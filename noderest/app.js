const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/ninja", { useNewUrlParser: true });
app.use(bodyParser.json());
app.use("/api", require("./routers/api"));
app.use(function(err, req, res, next) {
  res.status(422).send({ error: err.message });
});
app.listen(3000, function() {
  console.log("Listening");
});
