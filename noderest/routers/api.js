const express = require("express");
const router = express.Router();
const Ninja = require("../models/ninja");
router.get("/ninjas", function(req, res, next) {
  Ninja.find({}).then(function(result) {
    res.send(result);
  });
});
router.post("/ninjas", function(req, res, next) {
  Ninja.create(req.body)
    .then(function(result) {
      res.send(result);
    })
    .catch(next);
});
router.put("/ninjas/:id", function(req, res, next) {
  Ninja.findById({ _id: req.params.id }).then(function(result) {
    if (result != null) {
      Ninja.findByIdAndUpdate(req.params.id, req.body, { new: true }).then(
        function(result) {
          res.send(result);
        }
      );
    } else {
      res.send({ error: "id not found" });
    }
  });
});
router.delete("/ninjas/:id", function(req, res, next) {
  Ninja.findById({ _id: req.params.id }).then(function(result) {
    if (result != null) {
      Ninja.findByIdAndDelete({ _id: req.params.id }).then(function(result) {
        res.send(result);
      });
    } else {
      res.send({ error: "id not found" });
    }
  });
});
module.exports = router;
